class aUserReceipt extends Section implements Initializable
{
    /**
     * @var \App\UserReceipt
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        app()->booted(function() {
            \AdminNavigation::getPages()->findById('menuClients')
                ->addPage(new Page(\App\Models\UserReceipt::class))
                    ->setIcon('fa fa-fax')
                    ->setPriority(300)
                    ->setAccessLogic(function(Page $page) {
                        return (auth()->user()->is('admin') || auth()->user()->is('director') || auth()->user()->is('accountant'));
                    })
                    ->addBadge(function() {
                        return \App\Models\UserReceipt::whereBetween('created_at', [Carbon::today(), Carbon::now()])->count();
                    });
        });

        $this->creating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Session::flash(
                'success_message', trans('messages.creating.success')
            );
            return true;
        });

        $this->created(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Session::flash(
                'info_message', trans('messages.created.info')
            );
            return true;
        });

        $this->updating(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Session::flash(
                'warning_message', trans('messages.updatind.warning')
            );
            return false;
        });

        $this->deleting(function($config, \Illuminate\Database\Eloquent\Model $model) {
            \Session::flash(
                'danger_message', trans('messages.deleting.danger')
            );
            return false;
        });
    }

/*
    ...
    ...
    ...
*/

}