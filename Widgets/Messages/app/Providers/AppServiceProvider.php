class AppServiceProvider extends ServiceProvider
{
    /**
     * Список виджетов, которые необходимо подключить в шаблоны
     *
     * @var array
     */
    protected $widgets = [
        \App\Widgets\DangerMessages::class,
        \App\Widgets\WarningMessages::class,
        \App\Widgets\InfoMessages::class
//        \SleepingOwl\Admin\Widgets\SuccessMessages::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Регистрация виджетов в реестре
        /** @var WidgetsRegistryInterface $widgetsRegistry */
        $widgetsRegistry = $this->app[\SleepingOwl\Admin\Contracts\Widgets\WidgetsRegistryInterface::class];

        foreach ($this->widgets as $widget) {
            $widgetsRegistry->registerWidget($widget);
        }

    }
/*
    ...
    ...
    ...
*/

}
