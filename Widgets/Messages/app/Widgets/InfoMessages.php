<?php

namespace App\Widgets;

use SleepingOwl\Admin\Widgets\Widget;
use AdminTemplate;

class InfoMessages extends Widget
{

    /**
     * Get content as a string of HTML.
     *
     * @return string
     */
    public function toHtml()
    {
//        return AdminTemplate::view('_partials.messages', [
        return view('admin.messages.info', [
            'messages' => session('info_message')
        ])->render();

    }

    /**
     * @return string|array
     */
    public function template()
    {
        return AdminTemplate::getViewPath('_layout.inner');
    }

    /**
     * @return string
     */
    public function block()
    {
        return 'content.top';
    }
}