<div role="tabpanel" class="tab-pane" id="comments">
	<table class="table table-striped table-hover table-bordered dashboard-table table_with_header">
		<tr>
			<th>{{ trans('labels.general.comment') }}</th>
			<th>{{ trans('labels.general.username') }}</th>
			<th>{{ trans('labels.general.created_at') }}</th>
		</tr>
		@foreach($comments as $comment)
			<tr>
				<td>{!! $comment['comment_source'] !!}</td>
				<td>{!! $comment['users']['name'] !!}</td>
				<td>{!! $comment['created_at'] !!}</td>
			</tr>
		@endforeach
	</table>
</div>