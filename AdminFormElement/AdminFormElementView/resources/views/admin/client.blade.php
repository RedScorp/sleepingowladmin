<div class="tab-content">
    <div role="tabpanel" class="tab-pane active">

        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.info.birthday') !!}</label>
                            <div class="form-control">
                                {!! $infoBirthday !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.info.marital') !!}</label>
                            <div class="form-control">
                                {!! $infoMarital !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.info.gender') !!}</label>
                            <div class="form-control">
                                {!! $infoGender !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.identify.identify') !!}</label>
                            <div class="form-control">
                                {!! $identifyName !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.identify.number') !!}</label>
                            <div class="form-control">
                                {!! $identifyNumber !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.identify.date') !!}</label>
                            <div class="form-control">
                                {!! $identifyDate !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.identify.authority') !!}</label>
                            <div class="form-control">
                                {!! $identifyAuthority !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.address.region') !!}</label>
                            <div class="form-control">
                                {!! $addressRegion !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.address.district') !!}</label>
                            <div class="form-control">
                                {!! $addressDistrict !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label">{!! trans('labels.user.address.city') !!}</label>
                            <div class="form-control">
                                {!! $addressName !!}
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.address.street') !!}</label>
                            <div class="form-control">
                                {!! $addressStreet !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.address.home') !!}</label>
                            <div class="form-control">
                                {!! $addressHome !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.address.housign') !!}</label>
                            <div class="form-control">
                                {!! $addressHousign !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('labels.user.address.flat') !!}</label>
                            <div class="form-control">
                                {!! $addressFlat !!}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        </div>

    </div>
</div>