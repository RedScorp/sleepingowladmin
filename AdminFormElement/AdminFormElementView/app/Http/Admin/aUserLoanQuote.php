class aUserLoanQuote extends Section implements Initializable
{
/*
    ...
    ...
    ...
*/

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $readonly = (UserLoanQuote::find($id)->status_id == 1 ? false : true);

        $form = AdminForm::panel();

        $form->addHeader([
                AdminFormElement::columns()
                    ->addColumn([
                        AdminFormElement::text('usersInfo.fullname', trans('labels.user.info.fullname'))->setReadOnly(true)
                    ], 3)->addColumn([
                        AdminFormElement::text('userIIN.iin', trans('labels.user.iin.iin'))->setReadOnly(true)
                    ], 3)->addColumn([
                        AdminFormElement::text('users.phone', trans('labels.user.profile.phone'))->setReadOnly(true)
                    ], 3)->addColumn([
                        AdminFormElement::text('users.email', trans('labels.user.profile.email'))->setReadOnly(true)
                    ])
        ]);

        $userLoanQuote = UserLoanQuote::find($id);
        $setData = [
            'infoBirthday' => $userLoanQuote->usersInfo->birthday,
            'infoMarital' => $userLoanQuote->usersRefMarital->name,
            'infoGender' => ($userLoanQuote->usersInfo->gender === 0 ? trans('labels.user.info.gender_woman') : trans('labels.user.info.gender_man')),
            'identifyName' => $userLoanQuote->usersRefIdentify->name,
            'identifyNumber' => $userLoanQuote->userIdentify->identify_number,
            'identifyDate' => $userLoanQuote->userIdentify->identify_date,
            'identifyAuthority' => $userLoanQuote->userIdentify->identify_authority,
            'addressRegion' => $userLoanQuote->userAddress->kato_region_name,
            'addressDistrict' => $userLoanQuote->userAddress->kato_district_name,
            'addressName' => $userLoanQuote->userAddress->kato_te_name,
            'addressStreet' => $userLoanQuote->userAddress->street,
            'addressHome' => $userLoanQuote->userAddress->home,
            'addressHousign' => $userLoanQuote->userAddress->housign,
            'addressFlat' => $userLoanQuote->userAddress->flat,
        ];
        $client = AdminFormElement::view('admin.client', $setData);

        $tabs = AdminDisplay::tabbed([
            trans('labels.general.card') => new \SleepingOwl\Admin\Form\FormElements([
                AdminFormElement::columns()
                    ->addColumn(function() use ($readonly) {
                        return [
                            AdminFormElement::text('quote', trans('labels.user.loanquote.quote'))
                                ->required()
                                ->setReadOnly($readonly),
                            AdminFormElement::select('status_id', trans('labels.general.status'))->setModelForOptions(new RefQuote)->setDisplay('name')
                                ->required()
                                ->setReadOnly($readonly),
                            AdminFormElement::text('comment', trans('labels.general.comment'))
                                ->setReadOnly($readonly),
                        ];
                    })
            ]),
            trans('labels.general.client') => new \SleepingOwl\Admin\Form\FormElements([
                $client,
            ]),
        ]);

        $form->addElement($tabs);
        $form->getButtons()->hideSaveAndCreateButton()->hideSaveAndCloseButton();

        return $form;

    }

/*
    ...
    ...
    ...
*/

}