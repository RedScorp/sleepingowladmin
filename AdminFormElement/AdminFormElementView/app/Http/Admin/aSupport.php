class aSupport extends Section implements Initializable
{
/*
    ...
    ...
    ...
*/

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $support = Support::find($id);

        $comments = $support->comments;
        $comments_tab = AdminFormElement::view('admin.comments', compact('comments'));

        $form = AdminForm::panel();
        $form->setHtmlAttribute('enctype', 'multipart/form-data');
        $form->addHeader(
            AdminFormElement::columns()
                ->addColumn([
                    AdminFormElement::date('created_at', trans('labels.general.created_at'))
                        ->setFormat('d-m-Y H:i:s')
                        ->setReadOnly(true),
                ], 3)->addColumn([
                    AdminFormElement::text('IssueName', trans('labels.general.issue'))
                        ->setReadOnly(true),
                ], 3)->addColumn([
                    AdminFormElement::text('users.name', trans('labels.general.username'))
                        ->setReadOnly(true),
                ], 3)->addColumn([
                    AdminFormElement::text('users.email', trans('labels.user.profile.email'))->setReadOnly(true)
                ], 3),
            AdminFormElement::text('title', trans('labels.general.title'))
                ->setReadOnly(true),
            AdminFormElement::html('<b>' . trans('labels.general.text') . '</b><br />')
        );
        $form->addBody(
            AdminColumn::text('text_source')
        );
        $form->addHeader(
            AdminFormElement::html('<br />'),
            AdminFormElement::html('<b>' . trans('labels.general.comment') . '</b><br />')
        );
        $form->addBody(
            $comments_tab
        );

        if ($support->supportStatus->name == 'closed' || $support->supportStatus->name == 'agreed') {
            $form->addBody(
                AdminFormElement::text('supportStatus.label', trans('labels.general.status'))
                    ->setReadOnly(true)
            );
            $form->getButtons()->hideSaveAndCreateButton()->hideSaveAndCloseButton()->hideDeleteButton();
        } else {
            $form->addBody(
                AdminFormElement::wysiwyg('comment_source', trans('labels.general.comment'), 'simplemde')
                    ->required()
                    ->disableFilter(),
                AdminFormElement::select('status_id', trans('labels.general.status'))->setModelForOptions(new RefSupport)->setDisplay('label')
                    ->required()
            );
            $form->getButtons()->hideSaveAndCreateButton()->hideSaveAndCloseButton();
        }
        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        $form = AdminForm::panel();
        $form->setHtmlAttribute('enctype', 'multipart/form-data');
        $form->addHeader(
            AdminFormElement::date('created_at', trans('labels.general.created_at'))
                ->setFormat('d-m-Y H:i:s')
                ->setDefaultValue(Carbon::now())
                ->required()
                ->setReadOnly(true),
            AdminFormElement::text('title', trans('labels.general.title'))
                ->required()
        );
        $form->addBody(
            AdminFormElement::wysiwyg('text_source', trans('labels.general.text'), 'simplemde')
                ->required()
                ->disableFilter()
        );
        $form->getButtons()->hideSaveAndCreateButton()->hideSaveAndCloseButton();

        return $form;
    }

/*
    ...
    ...
    ...
*/

}
